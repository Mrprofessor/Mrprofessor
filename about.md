---
layout: page
title: About
---

<p class="message">
  I can see nothing but void here.
</p>


I am an individual(*debatable*) from planet Earth with humongous curiosity in technology and practically zero talent.I love to
code as long as I know how to solve the problem and beyond that I run to [StackOverflow](https://stackoverflow.com) guys.Yeah I am a software developer(*yeah.. that means I edit excel sheets most of the time at work*) and my buddies call me Prof.

It all started in my 7th grade when I first watched Matrix.I really wanted to be a bullet dodging hacker.I should have taken the blue pill...well that's past.Then like all other kids I started playing games and planned to build one someday(*failed terribly*).By following *never get down* mantra I garaduated as a computer science engineer and setteled for an entry level developer job in an outsourcing company.Gotta meet Oracle someday I guess.

While I am not at a WorkStation I am probably sleeping or playing cricket.

You can find me on [Twitter](https://twitter.com/ThisIsRudra), on [GitHub](https://github.com/Mrprofessor) or at my home.

### Credits

<p class="message">
  All goes to me.
</p>

kidding....This site is built with the amazing [Jekyll](https://jekyllrb.com), a static site generator and proudly hosted on [GitHub Pages](https://pages.github.com).The layout of this site (Hyde theme) is designed [@mdo](https://github.com/mdo), the same guy who developed [Bootstrap](https://getbootstrap.com).The source code of this site is available as open source under MIT license and that means you can do whatever you want with this code.
