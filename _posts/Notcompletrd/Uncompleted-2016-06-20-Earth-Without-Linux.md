---
layout:	post
tilte:	Earth without Linux
---


Today was my first day as an [Outreachy](https://gnome.org/outreachy) intern with [Mozilla](https://www.mozilla.org)! What does that even mean? Why is it super exciting? How did I swing such a sweet gig? How will I be spending my summer non-vacation? Read on to find out!

[![Outreachy logo](https://outreachy.gnome.org/skins/easterngreen/css/img/outreachy-logo.png "Outreachy logo (outreachy.gnome.org)")](https://www.gnome.org/outreachy)

## What is Outreachy?

[Outreachy](https://gnome.org/outreachy) is a fantastic initiative to get more women and members of other underrepresented groups involved in Free & Open Source Software. Through Outreachy, organizations that create open-source software (e.g. [Mozilla](https://www.mozilla.org/Outreachy), [GNOME](https://wiki.gnome.org/Outreach/Outreachy), [Wikimedia](https://www.mediawiki.org/wiki/Outreachy/Round_12), to name a few) take on interns to work full-time on a specific project for 3 months. There are two internship rounds each year, May-August and December-March. Interns are paid for their time, and receive guidance/supervision from an assigned mentor, usually a full-time employee of the organization who leads the given project.

Oh yeah, and the whole thing is done remotely! For a lot of people (myself included) who don't/can't/won't live in a major tech hub, the opportunity to work remotely removes one of the biggest barriers to jumping in to the professional tech community. But as FOSS developers tend to be pretty distributed anyway (I think my project's team, for example, is on about 3 continents), it's relatively easy for the intern to integrate with the team. It seems that most communication takes place over  [IRC](https://developer.mozilla.org/en-US/docs/Mozilla/QA/Getting_Started_with_IRC) and, to a lesser extent, videoconferencing.

## What does an Outreachy intern do?

Anything and everything! Each project and organization is different. But in general, interns spend their time...

#### Coding (or not)
A lot of projects involve writing code, though what that actually entails (language, framework, writing vs. refactoring, etc.) varies from organization to organization and project to project. However, there are also projects that don't involve code at all, and instead have the intern working on equally important things like design, documentation, or community management.

As for me specifically, I'll be working on the project [Test-driven Refactoring of Marionette's Python Test Runner](https://wiki.mozilla.org/Outreachy#Test-driven_Refactoring_of_Marionette.27s_Python_Test_Runner_.5Bno_longer_taking_applicants.5D). You can click through to the project description for more details, but basically I'll be spending most of the summer writing Python code (yay!) to test and refactor a component of  [Marionette](https://developer.mozilla.org/en-US/docs/Mozilla/QA/Marionette), a tool that lets developers run automated Firefox tests. This means I'll be learning a lot about testing in general, Python testing libraries, the huge ecosystem of internal Mozilla tools, and maybe a bit about browser automation. That's a lot! Luckily, I have my mentor Maja (who happens to also be an alum of both Outreachy and RC!) to help me out along the way, as well as the other members of the Engineering Productivity team, all of whom have been really friendly & helpful so far.

#### Traveling
Interns receive a $500 stipend for travel related to Outreachy, which is fantastic. I intend, as I'm guessing most do, to use this to attend conference(s) related to open source. If I were doing a winter round I would totally use it to attend [FOSDEM](https://fosdem.org), but there are also a ton of conferences in the summer! Actually, you don't even need to do the traveling during the actual 3 months of the internship; they give you a year-long window so that if there's an annual conference you really want to attend but it's not during your internship, you're still golden.

At Mozilla in particular, interns are also invited to a week-long all-hands meet up! This is beyond awesome, because it gives us a chance to meet our mentors and other team members in person. (Actually, I doubly lucked out because I got to meet my mentor at RC during "Never Graduate Week" a couple of weeks ago!)

#### Blogging
One of the requirements of the internship is to blog regularly about how the internship and project are coming along. This is my first post! Though we're required to write a post every 2 weeks, I'm aiming to write one per week, on both technical and non-technical aspects of the internship. Stay tuned!

## How do you get in?

I'm sure every Outreachy participant has a different journey, but here's a rough outline of mine.

#### Step 1: Realize it is a thing
Let's not forget that the first step to applying for any program/job/whatever is realizing that it exists! Like most people, I think, I had never heard of Outreachy, and was totally unaware that a remote, paid internship working on FOSS was a thing that existed in the universe. But then, in the fall of 2015, I made one of my all-time best moves ever by attending the [Recurse Center](https://www.recurse.com) (RC), where I soon learned about Outreachy from various Recursers who had been involved with the program. I discovered it about 2 weeks before applications closed for the December-March 2015-16 round, which was pretty last-minute; but a couple of other Recursers were applying and encouraged me to do the same, so I decided to go for it!

#### Step 2: Frantically apply at last minute
Applying to Outreachy is a relatively involved [process](https://wiki.gnome.org/Outreachy#Application_Process). A couple months before each round begins, the list of participating organizations/projects is released. Prospective applicants are supposed to find a project that interests them, get in touch with the project mentor, and make an initial contribution to that project (e.g. fix a small bug).

But each of those tasks is pretty intimidating!
