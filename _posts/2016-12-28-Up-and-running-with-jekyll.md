---
layout:	post
tilte:	Up and Running with Jekyll
---

Finally the day has come when I have decided to leave wordpress for good.Wordpress had came as a revolutionary platform for bloggers.People had stopped writing the same static page to upload a simple post.Thanks to some static site generators who made it possible without any help from those slow and wary databases.Well this is how my journey started with Jekyll.

### Jekyll

Jekyll was created by Tom Preston-Werner, founder and former CEO of GitHub and ofcourse written in ruby.Well I guess that was something that kept me away from Jekyll for so long.Yeah..You got me, I am talking about Ruby.It's not like I hate ruby but did'nt find any reason to love it yet. Well for a guy who has to use three or four languages everyday to do his daily job,learning another would be the last thing he want to do.

But I was utterly wrong.Like I said it was written in ruby, It doesn't use ruby!!! And yeah It took me sometime to learn it (barely 4 hours) but when I did it was absoutely amazing.Well that's because all I had to do is add some more braces to the html file. Atleast that's what I felt.

### Jekyll and GitHub

If you had read the second paragraph you could easily imagine why these are real buddies. Well Later's co-founder developed the former one and probably It's GitHub who made it more famous.

There are a couple of reasons that made me take the decision to move my perfectly-working site (or kind of) to Jekyll and GitHub Pages:

#### 1. Free hosting

I still remember my sophomore years when I used to pay a remarkable amount of money from my pocket money to host some of my sites. Well that has changed now. GitHub provides absolutely free hosting for all kinds of static sites.[GitHub Pages](https://pages.github.io) are public webpages freely hosted and easily published through GitHub.

#### 2. No server side language or creepy databases

Maintaining a database for a simple blog is a real pain in the a..apple. I don't like to use database unless I really needed to. Your content is in safe hands....unless GitHub goes down.

#### 3. Add commit push

That's all you have to do in order to publish a post now. All you need is a text editor and git installed in your machine to update the site or release a blog post. Though you need to run a local server to test the core edits but not for a simple post.

#### 4. Blazing fast

Jekyll is really fast due to less dependencies.No need to add heavyweight libraries like Jquery and hence better speed comes with it.

### Setting up

I think it was one of the easiest set up I ever had for a fairly new concept. I manjaro linux so ruby was already up-to-date. All I had to to do is install jekyll bundle.

{% highlight terminal%}

$ gem install jekyll bundler
Successfully installed jekyll-3.4.3
Parsing documentation for jekyll-3.4.3
Installing ri documentation for jekyll-3.4.3
Done installing documentation for jekyll after 2 seconds
Successfully installed bundler-1.14.6
Parsing documentation for bundler-1.14.6
Done installing documentation for bundler after 5 seconds
2 gems installed
$
{% endhighlight %}

Here, we are installing the gem, creating the blog directory, and initialising the blog.

{% highlight terminal %}

$ jekyll new myblog
Running bundle install in /home/professor/Desktop/test/myblog...
...
...
New jekyll site installed in /home/professor/Desktop/test/myblog.
$ cd myblog
$ sudo jekyll build
$
{% endhighlight %}

After you’ve done this, youwill be able to see a demo blog, which you can serve up by running this command


{% highlight terminal %}

$ jekyll serve
Configuration file /home/professor/Desktop/test/myblog/_config.yml
Configuration file: /home/professor/Desktop/test/myblog/_config.yml
            Source: /home/professor/Desktop/test/myblog
       Destination: /home/professor/Desktop/test/myblog/_site
 Incremental build: disabled. Enable with --incremental
      Generating...
                    done in 0.356 seconds.
 Auto-regeneration: enabled for '/home/professor/Desktop/test/myblog'
Configuration file: /home/professor/Desktop/test/myblog/_config.yml
    Server address: http://127.0.0.1:4000/
  Server running... press ctrl-c to stop.
  ...
 __
{% endhighlight %}

Now if you open this url in a browser you will see this


![Blog](/img/jekyll_blog.png "Large example image")

### Conclusion

Jekyll is definitely an easy and awesome way to get started with your blog. It focuses on content can take care of itself. Check this guy out for more help [cheatsheet](http://ricostacruz.com/cheatsheets/jekyll.html).
